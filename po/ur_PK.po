# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-07-03 21:17+0200\n"
"PO-Revision-Date: 2017-09-19 18:06+0000\n"
"Last-Translator: Nick Schermer <nick@xfce.org>\n"
"Language-Team: Urdu (Pakistan) (http://www.transifex.com/xfce/xfce-panel-plugins/language/ur_PK/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ur_PK\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../src/smartbookmark.c:90
#, c-format
msgid "Failed to send %s to your preferred browser"
msgstr ""

#: ../src/smartbookmark.c:259
msgid "Smartbookmark"
msgstr "سمارٹ بُک مارک"

#: ../src/smartbookmark.c:264
msgid "Preferences"
msgstr "ترجیحات"

#. text label
#: ../src/smartbookmark.c:279
msgid "Label:"
msgstr "لیبل:"

#. size label
#: ../src/smartbookmark.c:294
msgid "Size:"
msgstr "حجم:"

#. Hide label option
#: ../src/smartbookmark.c:305
msgid "Hide label"
msgstr "لیبل چھپائیں"

#. url label
#: ../src/smartbookmark.c:316
msgid "URL:  "
msgstr "ربط:  "

#: ../src/smartbookmark.desktop.in.h:1
msgid "SmartBookmark"
msgstr "سمارٹ بُک مارک"

#: ../src/smartbookmark.desktop.in.h:2
msgid "Query websites from the Xfce panel"
msgstr "ایکسفس پینل کے لیے ویب سائٹ سے استفسار کریں"
